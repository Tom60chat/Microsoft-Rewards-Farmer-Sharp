using MicrosoftRewardsFarmer.Log.Enumerations;

namespace MicrosoftRewardsFarmer.Log.Types;

public abstract class BaseLogger
{
    #region Properties

    /// <summary>
    /// Where the log is written
    /// </summary>
    public string URI { get; protected set; }
    /// <summary>
    /// What should be written in
    /// </summary>
    public LoggerLevel[] Levels { get; protected set; }
    /// <summary>
    /// If the user wants to show the name in the log output
    /// </summary>
    public bool ShowNames { get; protected set; }

    #endregion

    #region Constructors

    protected BaseLogger(string uri, LoggerLevel[] levels, bool showNames)
    {
        URI = uri;
        Levels = levels;
        ShowNames = showNames;
    }

    #endregion

    #region Methods

    /// <summary>
    /// Write into the current logger
    /// </summary>
    /// <param name="content"></param>
    public abstract void Write(string content);

    #endregion
}
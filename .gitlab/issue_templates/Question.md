⚠️ This feature request respects the following points: ⚠️

- [ ] This question is not already asked on GitLab (I've searched it).
- [ ] The app is up to date.

What's your question?

/label ~Question

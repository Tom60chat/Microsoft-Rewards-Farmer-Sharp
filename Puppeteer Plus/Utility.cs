﻿using PuppeteerSharp;

namespace PuppeteerPlus;

public static class Utility
{
    // TODO This seam useless
    /// <summary>
    ///     Start the given browser and start a puppeteer session
    /// </summary>
    /// <param name="executablePath">The browser executable path</param>
    /// <param name="arguments">The browser arguments</param>
    /// <param name="headless">Start the browser in headless</param>
    /// <param name="userDataPath">The browser user data path</param>
    /// <returns>Return a new puppeteer browser session</returns>
    public static async Task<IBrowser> StartNewBrowserAsync(
        string executablePath, string[] arguments, bool headless = false, string userDataPath = "")
    {
        var browser = await Puppeteer.LaunchAsync(new LaunchOptions
        {
            ExecutablePath = executablePath,
            Args = arguments,
            Headless = headless,
            UserDataDir = userDataPath
        });

        return browser;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="userDataPath"></param>
    /// <exception cref="Exception"></exception>
    /// <returns></returns>
    public static void RemoveLastSession(string userDataPath)
    {
        var userSessionPath = Path.Combine(userDataPath, "Default/Sessions");
        
        if (!Directory.Exists(userSessionPath))
            return;
        
        var files = Directory.GetFiles(userSessionPath);

        foreach (var file in files)
            File.Delete(file);
    }

    /// <summary>
    ///     Try to get the user browser
    /// </summary>
    /// <param name="browserPath">The path to the user browser</param>
    /// <returns>If success</returns>
    public static bool TryGetUserBrowser(out string browserPath)
    {
        browserPath = string.Empty;
        
        var userBrowserPaths = Environment.OSVersion.Platform switch
        {
            PlatformID.Win32NT => new[]
            {
                // Google Chrome
                Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles),
                    "Google\\Chrome\\Application\\chrome.exe"
                ),
                // Chromium
                Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                    "Chromium\\Application\\chrome.exe"
                ),
                // Brave
                Path.Combine(
                    Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles),
                    "BraveSoftware\\Brave-Browser\\Application\\brave.exe"
                )
            },
            PlatformID.Unix => new[]
            {
                // TODO: which chrome

                // Linux
                "/usr/bin/chrome",
                "/usr/bin/chromium",
                "/usr/bin/chromium-browser",
                "/usr/bin/brave",
                "/usr/bin/brave-browser",
                "/usr/bin/brave-browser-stable",
                "/opt/brave.com/brave/brave",

                "/usr/local/bin/chrome",
                "/usr/local/bin/chromium",
                "/usr/local/bin/chromium-browser",
                "/usr/local/bin/brave-browser",
                "/usr/local/bin/brave-browser-stable",

                // MacOS
                "/Applications/Google Chrome Canary.app/Contents/MacOS/Google Chrome Canary",
                "/Applications/Google Chrome.app/Contents/MacOS/Google Chrome",
                "/Applications/Chromium.app/Contents/MacOS/Chromium",
                "/Applications/Brave.app/Contents/MacOS/Brave",

                // Brew
                "/opt/homebrew/bin/chromium",
                "/opt/homebrew/bin/chrome",
                "/opt/homebrew/bin/brave-browser"
            },
            _ => Array.Empty<string>()
        };

        foreach (var userBrowserPath in userBrowserPaths)
            if (File.Exists(userBrowserPath))
            {
                browserPath = userBrowserPath;
                return true;
            }

        return false;
    }

    /// <summary>
    ///     Try to get the puppeteer built-in browser
    /// </summary>
    /// <param name="browserPath">The path to the built-in browser</param>
    /// <returns>If success</returns>
    public static bool TryGetBuildInBrowser(out string browserPath)
    {
        browserPath = string.Empty;
        var browserFetcher = new BrowserFetcher();
        var revisions = browserFetcher.LocalRevisions();

        foreach(var revision in revisions)
        {
            browserPath = browserFetcher.GetExecutablePath(revision);

            return true;
        }

        return false;
    }

    /// <summary>
    /// Download the build-in browser
    /// </summary>
    /// <returns>browserPath</returns>
    public static async Task<string> DownloadBuiltInBrowserAsync()
    {
        var browserDownloaded = await new BrowserFetcher().DownloadAsync();

        return browserDownloaded.ExecutablePath;
    }
}
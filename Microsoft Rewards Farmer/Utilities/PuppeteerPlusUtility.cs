using PuppeteerPlus;

namespace MicrosoftRewardsFarmer.Utilities;

public static class PuppeteerPlusUtility
{
    /// <summary>
    ///     Get the currently installed chromium based browser, if not available download one
    /// </summary>
    /// <returns>The user or built-in browser</returns>
    public static async Task<string> GetBrowserAsync()
    {
        switch (Configuration.BrowserPath.ToLower())
        {
            case "built-in":
                // Skip
                break;

            case "user":
                if (Utility.TryGetUserBrowser(out var browserUserPath))
                    return browserUserPath;
                // else use the built-in one
                break;

            // User defined browser
            default:
                if (File.Exists(Configuration.BrowserPath))
                    return Configuration.BrowserPath;

                throw new FileNotFoundException("The given browser by the user was not found");
        }
        
        // Get Built-in browser (Last option)
        if (Utility.TryGetBuildInBrowser(out var browserAppPath))
            return browserAppPath;

        Console.WriteLine(Resources.Puppeteer.Downloading_browser + "...");

        return await Utility.DownloadBuiltInBrowserAsync();
    }
}
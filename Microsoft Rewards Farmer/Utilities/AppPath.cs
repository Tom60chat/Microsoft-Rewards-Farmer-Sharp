﻿namespace MicrosoftRewardsFarmer.Utilities;

public static class AppPath
{
    #region Methods

    /// <summary>
    ///     Get the full path of a file inside the app directory
    /// </summary>
    /// <param name="fileName">Relative path</param>
    /// <returns>The full path</returns>
    public static string GetFullPath(string fileName)
    {
        fileName = fileName.Replace('\\', '/');
        fileName = fileName.TrimStart('/');

        var baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
        var relativePath = Path.Combine(baseDirectory, fileName);
        var path = Path.GetFullPath(relativePath);

        return path;
    }

    #endregion
}
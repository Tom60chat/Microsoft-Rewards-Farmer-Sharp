﻿using System;
using MicrosoftRewardsFarmer.Utilities;

namespace MicrosoftRewardsFarmer.Models;

/// <summary>
///     Account
/// </summary>
[Serializable]
public class Account
{
    /// <summary>
    ///     Gets or sets the username to be used for authentication.
    /// </summary>
    public string Username { get; set; } = string.Empty;

    /// <summary>
    ///     Gets or sets the password to be used for authentication.
    /// </summary>
    public string Password { get; set; } = string.Empty;

    /// <summary>
    ///     Gets or sets the otp to be used for authentication.
    /// </summary>
    public string OTP { get; set; } = string.Empty;

    /// <summary>
    ///     Gets or sets the encoding used for decrypting the user password and OTP.
    /// </summary>
    public string Encoding { get; set; } = string.Empty;

    /// <summary>
    ///     Decode the credential
    /// </summary>
    public void Decode()
    {
        switch (Encoding.ToLower())
        {
            case "base64":
                Converter.Base64_to_string(Password);
                OTP = Converter.Base64_to_string(OTP);
                break;
        }

        Encoding = string.Empty;
    }
}
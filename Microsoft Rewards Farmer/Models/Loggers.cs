using MicrosoftRewardsFarmer.Log.Types;

namespace MicrosoftRewardsFarmer.Models;

[Serializable]
public class Loggers
{
    public DiscordLogger[] Discords { get; set; } = Array.Empty<DiscordLogger>();

    public FileLogger[] Files { get; set; } = Array.Empty<FileLogger>();

    public TelegramLogger[] Telegrams { get; set; } = Array.Empty<TelegramLogger>();

    public BaseLogger[] GetLoggers()
    {
        var loggers = new BaseLogger[Discords.Length + Files.Length + Telegrams.Length];
        
        Files.CopyTo(loggers, 0);
        Discords.CopyTo(loggers, Files.Length);
        Telegrams.CopyTo(loggers, Files.Length + Discords.Length);
        
        return loggers;
    }
}


/*[Serializable]
public class LoggerModel
{
    [DefaultValue(LoggerType.None)]
    [JsonProperty(DefaultValueHandling = DefaultValueHandling.Populate, NullValueHandling = NullValueHandling.Include)]
    public LoggerType Type { get; set; } = LoggerType.None;

    public string URI { get; set; } = string.Empty;
    public LoggerLevel[]? Levels { get; set; } = Array.Empty<LoggerLevel>();
    public bool ShowNames { get; set; } = true;
}*/
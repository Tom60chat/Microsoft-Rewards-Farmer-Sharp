namespace MicrosoftRewardsFarmer.Exceptions;

public class SettingsLoadException : Exception
{
    public SettingsLoadException()
    {
    }

    public SettingsLoadException(string message)
        : base(message)
    {
    }

    public SettingsLoadException(string message, Exception inner)
        : base(message, inner)
    {
    }
}
﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Reflection.PortableExecutable;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using MicrosoftRewardsFarmer.Exceptions;
using MicrosoftRewardsFarmer.Log;
using MicrosoftRewardsFarmer.Log.Enumerations;
using MicrosoftRewardsFarmer.Log.Types;
using MicrosoftRewardsFarmer.Models;
using MicrosoftRewardsFarmer.Utilities;
using Newtonsoft.Json;
using EzCsl = Ez_console.Console;
using FC = Ez_console.Foreground_color;
using SharedResources = MicrosoftRewardsFarmer.Resources;

namespace MicrosoftRewardsFarmer.TUI;

internal static class Program
{
    #region Properties

    private static Settings _settings = new Settings();

    private static Logger _logger = new Logger(new[]
    {
        new FileLogger(AppPath.GetFullPath("Logs"),
                       new[] { LoggerLevel.Exception, LoggerLevel.Log, LoggerLevel.Summary }, false)
    });

    #endregion

    #region Methods

    private static void Main(string[] args)
    {
        try
        {
            AppOptions.Apply(args);

            _settings = Settings.GetSettings(Configuration.SettingsFilePath) ?? _settings;

            // If their is not logger, we keep the default one
            if (_settings.Loggers != null)
                try
                {
                    _logger = new Logger(_settings.Loggers.GetLoggers());
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);

                    Exit();
                }

            StartFarming();

            if (!AppOptions.Batch)
            {
                Console.WriteLine();
                Console.WriteLine(Resources.Main.Press_any_key_to_exit);
                Console.ReadKey();
                Console.Clear();
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(Resources.Main.Unhandled_exception);
            Console.WriteLine(ex);
            _logger.Write(ex);

            Exit();
        }
    }
    
    private static async Task<bool> CheckUpdateAsync()
    {
        var github = new GitlabUpdater("Tom60chat", "Microsoft-Rewards-Farmer-Sharp");

        return await github.CheckNewerVersionAsync(true);
    }

    private static void StartFarming()
    {
        var amount = Configuration.FarmsLimit > 0 ? Configuration.FarmsLimit : _settings.Accounts!.Length;
        var browserPath = PuppeteerPlusUtility.GetBrowserAsync().Result;
        var farmers = new List<Farmer>(_settings.Accounts!.Length);
        var i = 0;

        // App display and farm init
        Console.Clear();

        DrawHeader();

        // Initialize farmer
        foreach (var account in _settings.Accounts)
        {
            var farm = new Farmer(i++, account, browserPath);
            farmers.Add(farm);
            WriteStatus(farm , new ProgressDetail(0).SetProgress(SharedResources.Farmer.Pending + "..."));
        }
        
        //farmers.AddRange(Settings.Accounts.Select(account => new Farmer(account, browserPath)));
        
        // Start farming
        Parallel.ForEachAsync(
                     farmers,
                     new ParallelOptions { MaxDegreeOfParallelism = amount },
                     async (farm, cancellationToken) =>
                     {
                         var progress = new Progress<ProgressDetail>();

                         progress.ProgressChanged += (sender, model) => { WriteStatus(farm, model); };

                         // Initialization
                         //await farm.InitAsync(browserPath);
                         // Farming
                         try
                         {
                             // TODO Make it more better, that doing a hacky way to print as summary
                             var summary = await farm.StartAsync(progress);
                             WriteStatus(farm, summary, LoggerLevel.Summary);
                         }
                         catch (Exception e)
                         {
                             Debug.WriteLine(e); // Send full error to debugger console
                             _logger.Write(e, farm.Id + ": Farmer");
                             // TODO this is bad
                             WriteStatus(farm, new ProgressDetail(0).SetProgress("[Exception] Farmer - " + e.Message, 0), LoggerLevel.Exception);
                         }
                     })
                .Wait();

        // Farming done
        Console.SetCursorPosition(0, _settings.Accounts.Length * 2 + AppOptions.HeaderOffset);

        Console.WriteLine();
        Console.WriteLine(Resources.Main.Accounts_has_finish);
    }

    /// <summary>
    ///     Print status to the console and log file
    /// </summary>
    /// <param name="message">Message to write</param>
    /// <param name="progress"></param>
    /// <param name="level"></param>
    /// <param name="farmer"></param>
    private static void WriteStatus(in Farmer farmer, in ProgressDetail progress, LoggerLevel level = LoggerLevel.Log)
    {
        var points = $"{farmer.UserPoints} " + MicrosoftRewardsFarmer.Resources.Farmer.Points.ToLower();
        var status = $"§r[§a{farmer.Name}§r](§b{progress.Current}/{progress.Total}§r) - §9{points}: §r{progress.Detail}";
        var cleanedStatus = Ez_console.Utility.Remove_color_code(status);

        EzCsl.ClearLine(farmer.Id * 2 + AppOptions.HeaderOffset);
        EzCsl.ClearLine(farmer.Id * 2 + AppOptions.HeaderOffset + 1);
        EzCsl.WriteToLine(status, farmer.Id * 2 + AppOptions.HeaderOffset, 2);

        // TODO Show name Logger
        // Replacing account name for the user privacy
        _logger.Write(
            cleanedStatus.Replace(farmer.Name, farmer.Id.ToString()),
            level
        );
#if DEBUG
        Debug.WriteLine(cleanedStatus);
#endif
    }

    /// <summary>
    /// Draw the header at the top of the console app
    /// </summary>
    private static void DrawHeader()
    {
        var update = Resources.Main.Checking_version + "...";        
        var leftHeader =
            $"{FC.red}Microsoft {FC.green}Rewards {FC.cyan}Farmer {FC.yellow}{Resources.Main.By.ToLower()} {FC.reset}Tom Olivier";
        var rightHeader = $"Version {Assembly.GetEntryAssembly()?.GetName().Version} ({update})";

        DrawHeader(leftHeader, rightHeader);

        // Check for new version in background
        Task.Run(async () =>
        {
            rightHeader = await CheckUpdateAsync()
                ? rightHeader.Replace(update, Resources.Main.New_version_available)
                : rightHeader.Replace(update, Resources.Main.Latest_version);

            DrawHeader(leftHeader, rightHeader);
        });
    }

    /// <summary>
    /// Draw the header at the top of the console app
    /// </summary>
    /// <param name="leftHeader">Left text</param>
    /// <param name="rightHeader">Right text</param>
    private static void DrawHeader(string leftHeader, string rightHeader)
    {
        int spaces = Console.BufferWidth -
                 (Ez_console.Utility.Remove_color_code(leftHeader).Length + rightHeader.Length);

        if (spaces < 1)
        {
            AppOptions.HeaderOffset = 3;
            EzCsl.WriteToLine(leftHeader, 0, 2, false);
            EzCsl.WriteToLine(rightHeader, 1, 1, false);

            _logger.Write(Ez_console.Utility.Remove_color_code(leftHeader));
            Debug.WriteLine(leftHeader);
            _logger.Write(Ez_console.Utility.Remove_color_code(rightHeader));
            Debug.WriteLine(rightHeader);
        }
        else
        {
            AppOptions.HeaderOffset = 2;
            string header = leftHeader + new string(' ', spaces) + rightHeader;

            EzCsl.WriteToLine(header, 0, 2, false);

            _logger.Write(Ez_console.Utility.Remove_color_code(header));
            Debug.WriteLine(header); ;
        }
    }

    /// <summary>
    ///     Wait for user to press a key if AppOptions.Batch is false,
    ///     and terminates this process and returns an exit code 1 to the operating system.
    /// </summary>
    private static void Exit()
    {
        if (!AppOptions.Batch)
        {
            Console.WriteLine();
            Console.WriteLine(Resources.Main.Press_any_key_to_exit);
            Console.ReadKey();
        }

        Environment.Exit(1);
    }

    #endregion
}
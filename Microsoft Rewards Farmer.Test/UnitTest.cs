using System;
using System.Threading.Tasks;
using MicrosoftRewardsFarmer.Models;
using MicrosoftRewardsFarmer.Utilities;
using PuppeteerSharp;
using Xunit.Abstractions;

namespace MicrosoftRewardsFarmer.Test;

public abstract class UnitTest
{
    #region Variables

    protected readonly ITestOutputHelper output;

    #endregion

    #region Constructors

    protected UnitTest(ITestOutputHelper output)
    {
        Configuration.Headless = false;
        Configuration.Profil = false;
        Configuration.Unit_test = true;
        //Configuration.BrowserPath = "built-in";
        this.output = output;
    }

    #endregion

    #region Methods

    protected static async Task<IBrowser> GetBrowserAsync()
    {
        var browser = await PuppeteerPlus.Utility.StartNewBrowserAsync(
            await PuppeteerPlusUtility.GetBrowserAsync(),
            Configuration.BrowserArguments,
            Configuration.Headless);

        ExternalProcessManagement.RegisterForClosing(browser.Process);

        return browser;
    }


    protected static async Task<IPage> GetNewPageAsync()
    {
        var browser = await GetBrowserAsync();
        var page = await browser.NewPageAsync();

        page.Error += async (sender, args) =>
        {
            await page.ReloadAsync();
        };

        return page;
    }

    protected static Account GetAccount()
    {
        var configs = Settings.GetSettings(Configuration.SettingsFilePath);

        return configs.Accounts[0];
    }

    protected Progress<string> GetProgress()
    {
        var progress = new Progress<string>();

        progress.ProgressChanged += (_, value) =>
            output.WriteLine(value);

        return progress;        
    }
    #endregion
}
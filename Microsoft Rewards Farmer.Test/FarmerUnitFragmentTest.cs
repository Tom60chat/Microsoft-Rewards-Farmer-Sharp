using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using MicrosoftRewardsFarmer.Models;
using MicrosoftRewardsFarmer.Puppeteer;
using MicrosoftRewardsFarmer.TheFarm;
using PuppeteerSharp;
using Xunit;
using Xunit.Abstractions;

namespace MicrosoftRewardsFarmer.Test;

public abstract class FarmerUnitFragmentTest
{
    #region Constructors

    protected FarmerUnitFragmentTest(ITestOutputHelper output)
    {
        AppOptions.Unit_test = true;

        _output = output;
        _farmer = new Farmer(GetAccount(), 0);
    }

    #endregion

    #region Variables

    private readonly ITestOutputHelper _output;
    private readonly Farmer _farmer;

    #endregion

    #region Methods

    private static AccountModel GetAccount()
    {
        var settings = Settings.GetSettings(AppOptions.Settings_file_path);

        if (settings == null)
            throw new Exception("Missing settings");
        if (settings.Accounts == null)
            throw new Exception("Missing accounts");
        return settings.Accounts[0];
    }

    private async Task FastLogin()
    {
        var connected = false;
        var session = new Session(_farmer.Name, _farmer.MainPage);
        
        if (session.Exists())
            if (await Bing.GoToBingAsync(_farmer.MainPage))
            {
                await session.RestoreAsync();

                if (await _farmer.MainPage.TryGoToAsync(MsRewards.URL, WaitUntilNavigation.Networkidle0))
                    connected = await session.RestoreAsync();
            }

        if (!connected)
            await Bing.SignInToMicrosoftAsync(_farmer.MainPage, _farmer.Account, new Progress<string>());

        _output.WriteLine("Logged as " + _farmer.Name);
    }

    private async Task Init()
    {
        var browserPath = await PuppeteerPlusUtility.GetBrowserAsync();
        await _farmer.InitAsync(browserPath);
    }

    [Fact]
    public void TestSettings()
    {
        var settings = Settings.GetSettings(AppOptions.Settings_file_path);

        Assert.NotNull(settings);

        AppOptions.Apply(new[] { "-s", "Settings2.json" });

        File.WriteAllText("Settings2.json", "{}");

        var settings2 = Settings.GetSettings(AppOptions.Settings_file_path);

        Assert.NotNull(settings2);
    }

    [Fact]
    public async Task TestGoTo()
    {
        var n = 2;
        var tasks = new Task[n];
        Task task;

        var browserPath = await PuppeteerPlusUtility.GetBrowserAsync();
        //await _farmer.InitAsync(browserPath);


        // TODO update
        /*for (var i = 0; i < n; i++)
        {
            var page = await _farmer.Browser.NewPageAsync();

            task = Task.Run(async () =>
            {
                var n = 100;

                for (var i = 0; i < n; i++) await page.TryGoToAsync(Bing.URL);
            });
            tasks[i] = task;
        }
        */

        Task.WaitAll(tasks);
    }

    [Fact]
    public async Task TestLogin()
    {
        await Init();
        await Bing.SignInToMicrosoftAsync(_farmer.MainPage, _farmer.Account, new Progress<string>());
        await _farmer.MainPage.GoToAsync(Bing.URL);
    }

    [Fact]
    public async Task TestSaveSession()
    {
        await Init();
        await Bing.SignInToMicrosoftAsync(_farmer.MainPage, _farmer.Account, new Progress<string>());
        var session = new Session(_farmer.Name, _farmer.MainPage);

        Assert.True(await Bing.GoToBingAsync(_farmer.MainPage));
        await session.SaveAsync();
        Assert.True(await _farmer.MainPage.TryGoToAsync(MsRewards.URL, WaitUntilNavigation.Networkidle0));
        await session.SaveAsync();
    }

    [Fact]
    public async Task TestRestoreSession()
    {
        await Init();
        var session = new Session(_farmer.Name, _farmer.MainPage);

        Assert.True(await Bing.GoToBingAsync(_farmer.MainPage));
        Assert.True(await session.RestoreAsync());
        Assert.True(await _farmer.MainPage.TryGoToAsync(MsRewards.URL, WaitUntilNavigation.Networkidle0));
        Assert.True(await session.RestoreAsync());
        Assert.True(await MsRewards.GetRewardsPointsAsync(_farmer.MainPage, new Progress<string>()) > 0);
    }

    [Fact]
    public async Task TestGetRewardsPoints()
    {
        await Init();
        await FastLogin();
        await MsRewards.GoToMicrosoftRewardsPageAsync(_farmer.MainPage);
        var points = await MsRewards.GetRewardsPointsAsync(_farmer.MainPage, new Progress<string>());
        _output.WriteLine(points.ToString());
    }

    [Fact]
    public async Task TestGetSearchPoints()
    {
        await Init();
        await FastLogin();
        await MsRewards.GoToMicrosoftRewardsPageAsync(_farmer.MainPage);
        var points = await MsRewards.GetRewardsSearchPointsAsync(_farmer.MainPage, new Progress<string>());
        _output.WriteLine(points.ToString());
    }

    [Fact]
    public async Task TestGetCards()
    {
        await Init();
        await FastLogin();
        await MsRewards.GetCardsAsync(_farmer.MainPage, new Progress<string>());
    }

    [Fact]
    public async Task TestProceedCard()
    {
        await Init();

        Dictionary<string, string> cards = new()
        {
            {
                "Quiz",
                Bing.URL +
                "search?q=Nouvelles%20technologies&rnoreward=1&mkt=FR-FR&FORM=ML12JG&skipopalnative=true&rqpiodemo=1&filters=BTEPOKey:%22REWARDSQUIZ_FRFR_MicrosoftRewardsQuizCB_20211130%22%20BTROID:%22Gamification_DailySet_FRFR_20211130_Child2%22%20BTROEC:%220%22%20BTROMC:%2230%22"
            },
            {
                "Quick quiz",
                Bing.URL +
                "search?q=qu%27est-ce%20que%20Z%20Event&rnoreward=1&mkt=FR-FR&FORM=ML12JG&skipopalnative=true&rqpiodemo=1&filters=BTEPOKey:%22REWARDSQUIZ_FRFR_MicrosoftRewardsQuizDS_20211201%22%20BTROID:%22Gamification_DailySet_FRFR_20211201_Child2%22%20BTROEC:%220%22%20BTROMC:%2230%22"
            },
            {
                "50/50",
                Bing.URL +
                "search?q=langue%20fran%c3%a7aise&rnoreward=1&mkt=FR-FR&FORM=ML12JG&skipopalnative=true&rqpiodemo=1&filters=BTEPOKey:%22REWARDSQUIZ_FR-FR_ThisOrThat_FrenchLangCountries_EB_20211129%22%20BTROID:%22Gamification_DailySet_FRFR_20211129_Child2%22%20BTROEC:%220%22%20BTROMC:%2250%22%20BTROQN:%220%22"
            },
            {
                "Poll",
                Bing.URL +
                "search?q=forets%20en%20france&rnoreward=1&mkt=FR-FR&skipopalnative=true&form=ML17QA&filters=PollScenarioId:%22POLL_FRFR_RewardsDailyPoll_20211207%22%20BTROID:%22Gamification_DailySet_FRFR_20211207_Child3%22%20BTROEC:%220%22%20BTROMC:%2210%22"
            },
            {
                "Xbox store",
                "https://www.microsoft.com/fr-fr/store/b/xbox?ocid=MSRewards&form=ML27YL&OCID=ML27YL&PUBL=RewardsDO&PROGRAMNAME=MicrosoftStore&CREA=ML27YL"
            },
            {
                "Weekly Quiz",
                Bing.URL +
                "search?q=Bing%20homepage%20quiz&rnoreward=1&mkt=EN-US&skipopalnative=true&form=ML17QA&filters=BTROID:%22Gamification_DailySet_20230114_Child2%22%20BTROEC:%220%22%20BTROMC:%2210%22"
            }
        };

        foreach (var card in cards)
        {
            await _farmer.MainPage.TryGoToAsync(
                card.Value,
                WaitUntilNavigation.Networkidle0);
            await MsRewards.ProceedCardAsync(_farmer.MainPage);
        }
    }

    [Fact]
    public async Task RunDesktopSearchesTest()
    {
        await Init();

        await Bing.SwitchToDesktopAsync(_farmer.MainPage);
        await Bing.RunSearchesAsync(_farmer.MainPage, 10, new Progress<string>());

        await _farmer.StopAsync();
    }

    [Fact]
    public async Task RunMobileSearchesTest()
    {
        await Init();

        await Bing.SwitchToMobileAsync(_farmer.MainPage);
        await Bing.RunSearchesAsync(_farmer.MainPage, 10, new Progress<string>());

        await _farmer.StopAsync();
    }

    [Fact]
    public void RunRandomWordGenTest()
    {
        var words = RandomWord.GetWords(10);

        _output.WriteLine(string.Join('\n', words));

        var dictionaryFile = new FileInfo("Dictionary.txt");

        if (dictionaryFile.Exists)
            dictionaryFile.MoveTo("Dictionary.txt.disabled", true);

        words = RandomWord.GetWords(10);

        _output.WriteLine(string.Join('\n', words));

        if (dictionaryFile.Exists)
            dictionaryFile.MoveTo("Dictionary.txt");
    }

    [Fact]
    public async Task SwitchTest()
    {
        await Init();
        await Bing.RunSearchesAsync(_farmer.MainPage, 1, new Progress<string>());

        await Bing.SwitchToMobileAsync(_farmer.MainPage);
        await Task.Delay(250);
        await Bing.SwitchToDesktopAsync(_farmer.MainPage);

        await _farmer.StopAsync();
    }

    /*[Fact]
    public async Task BingIsConnectedTest()
    {
        await Init();
        await FastLogin();
        
        await Bing.GoToBingAsync(_farmer.MainPage);

        var result = await Bing.IsConnected(_farmer.MainPage);
        
        Assert.False(result);
    }*/
    #endregion
}